# i2dash-deploy

A docker container for i2dash deployments.

## Running the container

```
docker run -d -p 172.16.12.92:3838:3838 -v /path/to/workspace:/srv/shiny-server docker.gitlab.gwdg.de/loosolab/container/i2dash.deployment:r3.6.3_bioc3.10
```

## Building the container

```
docker build --squash -t docker.gitlab.gwdg.de/loosolab/container/i2dash.deployment:r3.6.3_bioc3.10 https://gitlab.gwdg.de/loosolab/container/i2dash.deployment.git
```
